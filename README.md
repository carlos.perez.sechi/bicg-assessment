# Amazon Web Scrapping

A web scraping and price featuring of [Gimnasios en Casa](https://www.amazon.com.mx/b?ie=UTF8&node=9788490011)

## Objective

This project has the objective of scraping about 100 products from Gimnasios en Casa section, and calculate the mean, mode, standard deviation and outliers prices for each product category of the products you have fetched.

## Prerequisites

Prerequisites necessary to run the project code:
- Python 3.6 or greater.
  
You may also need some of the following python3 libraries, which should be installed using pip3:
- flask
- Numpy
- Pandas
- BeautifulSoup
- Requests
  
## Execution
 
There may be two different parts:

### Scrapping

This part of the project would fetch all the necesary data about the products from the Amazon's web site. To execute the web scraper, you may have to do the following:
```bash
  python3 amazon_web_scrap.py csv_output_path
```
This script would automatically get at least 100 products from Amazon's aforementioned section, unless it fails, in which case it may get as many products as it could. csv_output_path would be a string that the program uses to save the csv with the results.

### Publication

To access the feature results, we ask you to build a Web (HTTP) Api with a flask implementation of these methods:

- GET /products/price/mean -> returning a json array with the mean price of each product category fetched in the scrapping step
- GET /products/price/sd -> returning a json array with the standard deviation for the price of each product category fetched in the scrapping step
- GET /products/price/mode -> returning a json array with the mode price of each product category fetched in the scrapping step
- GET /products/price/outliers -> returning a json array with the price ouliers of each product category fetched in the scrapping step

This Api should be authenticated by at least one OAuth v2 public provider. Please chose the one you prefer: Github, Google Accounts, Microsoft Identity Platform, Facebook Login, Twitch Authentication, etc.


## Evaluation

The objectives and the aspects to be evaluated are few and simple: the cleanliness of the code, HTTP Api security, knowledge and handling of advanced programming concepts in Python, the optimization of the algorithm and structures that you use, and the documentation of the code.
